<?php

namespace App\Http\Requests;

use App\Rules\MusclePartDestroy;
use Illuminate\Foundation\Http\FormRequest;

class MusclePartDestroyRequest extends FormRequest
{
    private $id;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'id' => \Request::instance()->id
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', new MusclePartDestroy()]
        ];
    }
}
