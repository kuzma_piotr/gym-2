<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'api_token' => 'required|size:60|exists:App\Models\User,api_token'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'api_token.required' => 'Podany API token jest niepoprawny',
            'api_token.size' => 'Podany API token jest niepoprawny',
            'api_token.exists' => 'Podany API token jest niepoprawny',
        ];
    }
}
