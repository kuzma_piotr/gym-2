<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResponseController extends Controller
{
    /**
     * Set error message
     *
     * @param $message
     * @param $httpStatus
     * @param null $error
     * @return \Illuminate\Http\JsonResponse
     */
    public static function error($message, $httpStatus, $error = NULL)
    {
        if($httpStatus == 422){
            $error = 'The given data was invalid.';
        }
        if($httpStatus == 401){
            $error = 'No access to resource.';
        }
        if($httpStatus == 403){
            $error = 'Forbidden.';
        }
        if($httpStatus == 404){
            $error = 'Not Found.';
        }

        return response()->json(
            [
                'message' => $error,
                'errors' => [
                    'error' => [
                        $message
                    ]
                ]
            ],
            $httpStatus
        );
    }

    /**
     * Set success message
     *
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public static function success($message, $status = 200)
    {
        return response()->json(['success' => $message], $status);
    }
}
