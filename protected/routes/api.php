<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\MusclePartsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// auth
Route::post('login', [AuthController::class, 'login']);
Route::get('logout', [AuthController::class, 'logout']);

// muscle parts
Route::prefix('muscle-parts')->group(function () {
    Route::get('/', [MusclePartsController::class, 'index']);
    Route::post('/', [MusclePartsController::class, 'store']);
    Route::get('{id}', [MusclePartsController::class, 'show']);
    Route::put('/{id}', [MusclePartsController::class, 'update']);
    Route::delete('{id}/delete', [MusclePartsController::class, 'destroy']);
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
