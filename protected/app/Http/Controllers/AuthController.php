<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Resources\UserResource;
use App\Models\User;

class AuthController extends Controller
{
    /**
     * Login
     *
     * @param LoginRequest $request
     * @return UserResource
     */
    public function login(LoginRequest $request)
    {
        $user = User::where('api_token', '=', $request->api_token)->firstOrFail();
        auth()->loginUsingId($user->id);

        return new UserResource(auth()->user());
    }

    /**
     * Logout
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return ResponseController::success('Poprawnie wylogowano', 200);
    }
}
