<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    public function testLoginApiTokenRequired()
    {
        $response = $this->postJson(url('/api/login'), ['api_token' => '']);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'api_token'
            ]
        ]);
    }

    public function testLoginApiTokenSize()
    {
        $response = $this->postJson(url('/api/login'), ['api_token' => '12345']);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'api_token'
            ]
        ]);
    }

    public function testLoginApiTokenExsist()
    {
        $response = $this->postJson(url('/api/login'), ['api_token' => 'a88upvbbjqvyz0wtwpeudx8h4r2rsxcs5qjvcq79i9oohs1mp28uto7a6bh0']);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'api_token'
            ]
        ]);
    }

    public function testLogin()
    {
        $user = User::factory()->create();

        $response = $this->postJson(url('/api/login'), ['api_token' => $user->api_token]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'email'
            ]
        ]);

        $user->delete();
    }

    public function testLogout()
    {
        $response = $this->get(url('/api/logout'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'success'
        ]);
    }
}
