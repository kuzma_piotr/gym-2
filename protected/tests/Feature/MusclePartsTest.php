<?php

namespace Tests\Feature;

use App\Models\MusclePart;
use Illuminate\Support\Str;
use Tests\TestCase;

class MusclePartsTest extends TestCase
{
    public function testIndex()
    {
        $response = $this->get(url('/api/muscle-parts'));
        $response->assertStatus(200);
    }

    public function testShow()
    {
        $item = MusclePart::factory()->create();

        $response = $this->get(url('/api/muscle-parts', $item->id));
        $response->assertStatus(200);
        $response->assertJsonCount(1);

        $item->delete();
    }

    public function testStoreValidation()
    {
        $response = $this->postJson(url('/api/muscle-parts'), ['name' => '']);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'name'
            ]
        ]);
    }

    public function testStore()
    {
        $name = Str::random(10);

        $response = $this->postJson(url('/api/muscle-parts'), ['name' => $name]);
        $response->assertStatus(201);
        $response->assertJsonStructure([
            'success'
        ]);

        MusclePart::where('name', '=', $name)->delete();
    }

    public function testUpdateValidation()
    {
        $item = MusclePart::factory()->create();

        $response = $this->putJson(url('/api/muscle-parts', $item->id), ['name' => '']);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'name'
            ]
        ]);

        $item->delete();
    }

    public function testUpdate()
    {
        $item = MusclePart::factory()->create();

        $response = $this->putJson(url('/api/muscle-parts', $item->id), ['name' => Str::random(10).'1']);
        $response->assertJsonStructure([
            'success'
        ]);

        $item->delete();
    }

    public function testDestroy()
    {
        $item = MusclePart::factory()->create();

        $response = $this->deleteJson(url('/api/muscle-parts/'.$item->id.'/delete'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'success'
        ]);
    }

}
