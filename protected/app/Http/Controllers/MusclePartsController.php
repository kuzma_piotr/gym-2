<?php

namespace App\Http\Controllers;

use App\Http\Requests\MusclePartDestroyRequest;
use App\Http\Requests\MusclePartStoreRequest;
use App\Http\Requests\MusclePartUpdateRequest;
use App\Http\Resources\MusclePartResource;
use App\Models\MusclePart;

class MusclePartsController extends Controller
{
    public function index()
    {
        return MusclePartResource::collection(MusclePart::all());
    }

    public function show($id)
    {
        return new MusclePartResource(MusclePart::findOrFail($id));
    }

    public function store(MusclePartStoreRequest $request)
    {
        $item = new MusclePart();
        $item->name = $request->input('name');
        $item->save();

        return ResponseController::success('Partia mięśniowa została dodana', 201);
    }

    public function update(MusclePartUpdateRequest $request, $id)
    {
        $item = MusclePart::findOrFail($id);
        $item->name = $request->input('name');
        $item->save();

        return ResponseController::success('Partia mięśniowa została zaktualizowana', 200);
    }

    public function destroy(MusclePartDestroyRequest $request, $id)
    {
        $item = MusclePart::findOrFail($id);
        $item->delete();

        return ResponseController::success('Partia mięśniowa została usunięta', 200);
    }
}
