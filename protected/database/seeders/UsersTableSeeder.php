<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['Piotr Kuźma', 'admin@piotrkuzma.pl', 'qwerty12345', '0syHnl0Y9jOIfszq11EC2CBQwCfObmvscrZYo5o2ilZPnohvndH797nDNyAT']
        ];

        foreach($items as $item){
            DB::table('users')->insert([
                'name' => $item[0],
                'email' => $item[1],
                'email_verified_at' => now(),
                'api_token' => $item[3],
                'password' => bcrypt($item[2]),
                'remember_token' => Str::random(10),
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
    }
}
