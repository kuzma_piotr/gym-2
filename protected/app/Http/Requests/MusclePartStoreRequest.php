<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MusclePartStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:App\Models\MusclePart,name'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Proszę wpisać nazwę patrii mięśniowej',
            'name.unique' => 'Podana partnia pięśniowa już istnieje'
        ];
    }
}
