<?php

namespace App\Rules;

// use App\Models\Exercise;
use Illuminate\Contracts\Validation\Rule;

class MusclePartDestroy implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // $exercise = Exercise::where('muscle_part_id', '=', \Request::instance()->id)->first();
        // return $exercise ? false : true;
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Nie można usunąć partii mięśniowej ponieważ są przypisane do niej ćwiczenia.';
    }
}
