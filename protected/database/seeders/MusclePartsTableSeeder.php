<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MusclePartsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = ['Klatka piersiowa', 'Barki', 'Plecy', 'Nogi', 'Triceps', 'Biceps', 'Brzuch', 'Przedramiona', 'Pośladki'];

        foreach($items as $item){
            DB::table('muscle_parts')->insert([
                'name' => $item,
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
    }
}
